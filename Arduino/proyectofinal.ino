#include <LiquidCrystal.h>
#include <SPI.h>
#include <MFRC522.h>
#define SS_PIN 10
#define RST_PIN 9

int currentammo=1;
bool disparar;
int ammoindex=1;
int sh=6;
int sh_state;
int xPin = A1;
int yPin = A0;
int shoots=0;
String key;

// Declare variables for positions:
int xPosition, yPosition;
LiquidCrystal lcd(0, 1, 2, 3, 4, 7);
#include <AddicoreRFID.h>
#include <SPI.h>

#define  uchar unsigned char
#define uint  unsigned int

uchar fifobytes;
uchar fifoValue;

AddicoreRFID myRFID; // create AddicoreRFID object to control the RFID module

/////////////////////////////////////////////////////////////////////
//set the pins
/////////////////////////////////////////////////////////////////////
const int chipSelectPin = 10;
const int NRSTPD = 5;

//Maximum length of the array
#define MAX_LEN 16
void setup() {
  Serial.begin(9600);
    SPI.begin();

  pinMode(chipSelectPin,OUTPUT);              // Set digital pin 10 as OUTPUT to connect it to the RFID /ENABLE pin 
    digitalWrite(chipSelectPin, LOW);         // Activate the RFID reader
  pinMode(NRSTPD,OUTPUT);                     // Set digital pin 10 , Not Reset and Power-down
    digitalWrite(NRSTPD, HIGH);

  myRFID.AddicoreRFID_Init();
    lcd.begin(16,2);
    pinMode(sh,INPUT);
    pinMode(xPin, INPUT);
    pinMode(yPin, INPUT);

}

void loop() {
  xPosition = analogRead(xPin);
  yPosition = analogRead(yPin);
  sh_state=digitalRead(sh);  
  lcd.print("X");
  printBullets();
  //shoot();
  if(sh_state==LOW && disparar==false)
  {
    shoots=1;
    disparar=true;
  }
  else if (sh_state==HIGH)
  {
    shoots=0;
    disparar=false;
  }
  UID();
  Serial.print(xPosition);
  Serial.print("/");  
  Serial.print(yPosition);
  Serial.print("/");   
  Serial.print(shoots);
  Serial.print("/");   
  Serial.print(ammoindex);  
  Serial.println("");
  
}

void printBullets()
{
  for (int i = 0; i < 24; i++)
  {
    lcd.setCursor(i,0);
    lcd.print(" ");
    lcd.setCursor(i,1);
    lcd.print(" ");
  }
  if(ammoindex==1)
  {
        lcd.setCursor(0,0);
        lcd.print("Pistol");    
    if(disparar)
    {
        disparar=false;

    }
    for(int i=0; i<16;i++)
    {
      
      lcd.setCursor(i,1);
      lcd.print("X"); 
    }       
  }
  else if(ammoindex==2)
  {
        lcd.setCursor(0,0);
        lcd.print("Shotgun");    
    if(disparar)
    {      
        disparar=false;

    }
    for(int i=0; i<16;i++)
    {
      lcd.setCursor(i,1);
      lcd.print("Z"); 
    }       
  }

}
void UID(){

      uchar i, tmp, checksum1;
  uchar status;
        uchar str[MAX_LEN];
        uchar RC_size;
        uchar blockAddr;  //Selection operation block address 0 to 63
        String mynum = "";

        str[1] = 0x4400;
  //Find tags
  status = myRFID.AddicoreRFID_Request(PICC_REQIDL, str); 

  //Anti-collision, return tag serial number 4 bytes
  status = myRFID.AddicoreRFID_Anticoll(str);
  if (status == MI_OK)
  {
          key = "";
          checksum1 = str[0] ^ str[1] ^ str[2] ^ str[3];

          key += str[0];
          key += str[1];
          key += str[2];
          key += str[3];

          if (str[4] == checksum1)
          {
            if(key == "4596133")
            {
                ammoindex = 1;
            }
            else if(key == "3820104172")
            {
                ammoindex = 2;
            }
  }

        myRFID.AddicoreRFID_Halt();
}
}
